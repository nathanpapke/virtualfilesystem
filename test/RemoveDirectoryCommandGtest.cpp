////////////////////////////////////////////////////////////////////////////////
/**
* @file RemoveDirectoryCommandGtest.cpp
* @brief A test of the RemoveDirectoryCommand class.
*
* (C) Copyright 2018 HP Development Company, L.P.
* All rights reserved.
*/
////////////////////////////////////////////////////////////////////////////////

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <memory>
#include <string>
#include <vector>

#include "RemoveDirectoryCommand.h"

using namespace virtualfilesystem;

class GivenRemoveDirectoryCommand : public ::testing::Test
{
    public:
        std::shared_ptr<RemoveDirectoryCommand> rd_ = std::make_shared<RemoveDirectoryCommand>();
        std::shared_ptr<std::vector<std::string>> tokens_ = std::make_shared<std::vector<std::string>>();
};

TEST_F(GivenRemoveDirectoryCommand, WhenGetNameCalled_ShouldReturnName)
{
    EXPECT_EQ(RemoveDirectoryCommand::getName(), "rd");
}

TEST_F(GivenRemoveDirectoryCommand, WhenExecuted_ShouldReturnTrue)
{
    tokens_->empty();
    EXPECT_TRUE(rd_->execute(*tokens_));
}
