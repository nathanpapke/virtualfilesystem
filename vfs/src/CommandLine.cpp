////////////////////////////////////////////////////////////////////////////////
/**
* @file CommandLine.cpp
* @brief A class to display a prompt and get a command.
*
* (C) Copyright 2018 HP Development Company, L.P.
* All rights reserved.
*/
////////////////////////////////////////////////////////////////////////////////

#include "../inc/CommandLine.h"

#include <iostream>

namespace virtualfilesystem {

void CommandLine::prompt()
{
    std::cout << "> ";
}

bool CommandLine::run(std::istream& stream)
{
    stream >> input_;
    
    if (input_ == "exit")
    {
        return false;
    }
    else if (input_ == "")
    {
        //std::cout << "> ";
        return true;
    }
    else
    {
        std::cout << "Your input was: " << input_ << "\n";
        return true;
    }
}

} // namespace virtualfilesystem
