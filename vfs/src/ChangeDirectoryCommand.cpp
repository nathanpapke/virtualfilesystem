////////////////////////////////////////////////////////////////////////////////
/**
* @file ChangeDirctoryCommand.cpp
* @brief A command for changing directories.
*
* (C) Copyright 2018 HP Development Company, L.P.
* All rights reserved.
*/
////////////////////////////////////////////////////////////////////////////////

#include "../inc/ChangeDirectoryCommand.h"

namespace virtualfilesystem {

std::string ChangeDirectoryCommand::getName()
{
    return "cd";
}

bool ChangeDirectoryCommand::execute(const std::vector<std::string>& command)
{
    // Check for no remaining tokens.
        // Give error message and return false.

    // Check for valid directory name.
        // Change directory pointer to specified directory.

    // Check for invalid directory name.
        // Give error message and return false.

    // Check for null string.
        // Return false.

    return false;
}

} // namespace virtualfilesystem
