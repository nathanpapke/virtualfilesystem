////////////////////////////////////////////////////////////////////////////////
/**
* @file MakeDirectoryCommandGtest.cpp
* @brief A test of the MakeDirectoryCommand class.
*
* (C) Copyright 2018 HP Development Company, L.P.
* All rights reserved.
*/
////////////////////////////////////////////////////////////////////////////////

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <memory>
#include <string>
#include <vector>

#include "MakeDirectoryCommand.h"

using namespace virtualfilesystem;

class GivenMakeDirectoryCommand : public ::testing::Test
{
    public:
        std::shared_ptr<MakeDirectoryCommand> md_ = std::make_shared<MakeDirectoryCommand>();
        std::shared_ptr<std::vector<std::string>> tokens_ = std::make_shared<std::vector<std::string>>();
};

TEST_F(GivenMakeDirectoryCommand, WhenGetNameCalled_ShouldReturnName)
{
    EXPECT_EQ(MakeDirectoryCommand::getName(), "md");
}

TEST_F(GivenMakeDirectoryCommand, WhenExecuted_ShouldReturnTrue)
{
    tokens_->empty();
    EXPECT_TRUE(md_->execute(*tokens_));
}
