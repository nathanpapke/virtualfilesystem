////////////////////////////////////////////////////////////////////////////////
/**
* @file ChangeDirctoryCommand.h
* @brief A command for changing directories.
*
* (C) Copyright 2018 HP Development Company, L.P.
* All rights reserved.
*/
////////////////////////////////////////////////////////////////////////////////

#ifndef CHANGEDIRECTORYCOMMAND_H
#define CHANGEDIRECTORYCOMMAND_H

#include "ICommand.h"

namespace virtualfilesystem {

/**
 * @brief A command for changing directories.
*/
class ChangeDirectoryCommand : public ICommand
{
    public:
        /**
         * @brief Constructor.
        */
        ChangeDirectoryCommand() = default;
        
        /**
         * @brief Destructor.
        */
        ~ChangeDirectoryCommand() = default;

        /**
         * @brief Returns the name of the command.
         * @return Name of command.
        */
        static std::string getName();

        /**
         * Executes the command.
         * @param command  [in] Name of command and arguments.
         * @return True if succeeded, false otherwise.
        */
        bool execute(const std::vector<std::string>& command);
};

} // namespace virtualfilesystem

#endif // CHANGEDIRECTORYCOMMAND_H
