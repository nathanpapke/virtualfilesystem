////////////////////////////////////////////////////////////////////////////////
/**
* @file main.cpp
* @brief A test of the command program functions.
*
* (C) Copyright 2018 HP Development Company, L.P.
* All rights reserved.
*/
////////////////////////////////////////////////////////////////////////////////

#include <gtest/gtest.h>

int main(int argc, char **argv)
{
    testing::InitGoogleTest();

    return RUN_ALL_TESTS();
}
