////////////////////////////////////////////////////////////////////////////////
/**
* @file CommandLine.h
* @brief A class to display a prompt and get a command.
*
* (C) Copyright 2018 HP Development Company, L.P.
* All rights reserved.
*/
////////////////////////////////////////////////////////////////////////////////

#ifndef COMMANDLINE_H
#define COMMANDLINE_H

#include <string>

namespace virtualfilesystem {

/**
 * @brief A class to display a prompt and get a command.
*/
class CommandLine
{
    public:
        /**
         * @brief Constructor.
        */
        CommandLine() = default;
        
        /**
         * @brief Destructor.
        */
        ~CommandLine() = default;

        /**
         * @brief Displays a prompt.
        */
        void prompt();

        /**
         * @brief Runs one command line entry.
         * @return True if succeeded, false otherwise.
        */
        bool run(std::istream& stream);

    private:
        /**
         * @brief Input text.
        */
        std::string input_;
};

} // namespace virtualfilesystem

#endif // COMMANDLINE_H
