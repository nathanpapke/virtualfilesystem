////////////////////////////////////////////////////////////////////////////////
/**
* @file MockCommand.cpp
* @brief A mock of the ICommand interface.
*
* (C) Copyright 2018 HP Development Company, L.P.
* All rights reserved.
*/
////////////////////////////////////////////////////////////////////////////////


#ifndef MOCK_ICOMMAND_H
#define MOCK_ICOMMAND_H

#include <gmock/gmock.h>

#include "../inc/ICommand.h"

namespace virtualfilesystem {

/**
 * @brief Mock ICommand class.
*/
class MockCommand : public ICommand
{
    public:
        MOCK_METHOD(std::string, getName, ());
        MOCK_METHOD(bool, execute, (const std::vector<std::string>&));
};

} // namespace virtualfilesystem

#endif // ICOMMAND_H
