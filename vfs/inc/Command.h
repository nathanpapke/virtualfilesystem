////////////////////////////////////////////////////////////////////////////////
/**
* @file Command.h
* @brief An enumeration for command classes.
*
* (C) Copyright 2018 HP Development Company, L.P.
* All rights reserved.
*/
////////////////////////////////////////////////////////////////////////////////

#ifndef COMMAND_H
#define COMMAND_H

namespace virtualfilesystem {

/**
 * @brief Interface for command classes.
*/
enum class Command
{
    None,
    List,
    MakeDirectory,
    RemoveDirectory,
    ChangeDirectory
};

} // namespace virtualfilesystem

#endif // COMMAND_H
