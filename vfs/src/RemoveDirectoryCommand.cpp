////////////////////////////////////////////////////////////////////////////////
/**
* @file RemoveDirectoryCommand.cpp
* @brief A command for removing directories.
*
* (C) Copyright 2018 HP Development Company, L.P.
* All rights reserved.
*/
////////////////////////////////////////////////////////////////////////////////

#include "../inc/RemoveDirectoryCommand.h"

namespace virtualfilesystem {

std::string RemoveDirectoryCommand::getName()
{
    return "rd";
}

bool RemoveDirectoryCommand::execute(const std::vector<std::string>& command)
{
    // Check for no remaining tokens.
        // Give error message and return false.

    // Check for valid directory name.
        // Ask for validation.
        // Remove specified directory.

    // Check for invalid directory name.
        // Give error message and return false.

    // Check for null string.
        // Return false.

    return false;
}

} // namespace virtualfilesystem
