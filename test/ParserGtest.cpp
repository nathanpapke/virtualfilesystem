////////////////////////////////////////////////////////////////////////////////
/**
* @file ParserGtest.cpp
* @brief A test of the Parser class.
*
* (C) Copyright 2018 HP Development Company, L.P.
* All rights reserved.
*/
////////////////////////////////////////////////////////////////////////////////

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <memory>

#include "Parser.h"

using namespace virtualfilesystem;

class GivenParser : public ::testing::Test
{
    public:
        std::shared_ptr<Parser> parser_ = std::make_shared<Parser>();
        std::shared_ptr<std::vector<std::string>> tokens_ = std::make_shared<std::vector<std::string>>();
};

TEST_F(GivenParser, WhenValidCommand_ShouldReturnTrue)
{
    tokens_->empty();
    tokens_->insert(tokens_->end(), "ls");
    EXPECT_TRUE(parser_->analyzeTokens(*tokens_));
}

// pass "    ls"
// pass "ls    "
// pass "foo ls bar"
// put in LexerGtest.cpp

TEST_F(GivenParser, WhenInvalidCommand_ShouldReturnFalse)
{
    tokens_->empty();
    tokens_->insert(tokens_->end(), "garbage");
    EXPECT_FALSE(parser_->analyzeTokens(*tokens_));
}
