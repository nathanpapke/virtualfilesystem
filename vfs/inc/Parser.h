////////////////////////////////////////////////////////////////////////////////
/**
* @file Parser.h
* @brief A parser to analyze tokens into a parse tree.
*
* (C) Copyright 2018 HP Development Company, L.P.
* All rights reserved.
*/
////////////////////////////////////////////////////////////////////////////////

#ifndef PARSER_H
#define PARSER_H

#include <memory>
#include <string>
#include <vector>

#include "ChangeDirectoryCommand.h"
#include "Command.h"
#include "ListCommand.h"
#include "MakeDirectoryCommand.h"
#include "RemoveDirectoryCommand.h"

namespace virtualfilesystem {

/**
 * @brief A parser to analyze tokens into a parse tree.
*/
class Parser
{
    public:
        /**
         * @brief Constructor.
        */
        Parser() = default;
        
        /**
         * @brief Destructor.
        */
        ~Parser() = default;

        /**
         * @brief Organizes the tokens into a parse tree.
         * @param tokens [in] Tokens to be parsed.
         * @return True if succeeded, false otherwise.
        */
        bool analyzeTokens(std::vector<std::string>& tokens);

    private:
        Command command_;
        std::string token_;

        /**
         * @brief Converts token to command.
         * @param token [in] Token to be converted.
         * @return Command to be executed.
        */
        std::unique_ptr<ICommand> convertTokenToCommand(const std::string& token); //proper way to reference family of classes?
};

} // namespace virtualfilesystem

#endif // PARSER_H
