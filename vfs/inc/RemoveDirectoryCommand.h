////////////////////////////////////////////////////////////////////////////////
/**
* @file RemoveDirectoryCommand.h
* @brief A command for removing directories.
*
* (C) Copyright 2018 HP Development Company, L.P.
* All rights reserved.
*/
////////////////////////////////////////////////////////////////////////////////

#ifndef REMOVEDIRECTORYCOMMAND_H
#define REMOVEDIRECTORYCOMMAND_H

#include "ICommand.h"

namespace virtualfilesystem {

/**
 * @brief A command for removing directories.
*/
class RemoveDirectoryCommand : public ICommand
{
    public:
        /**
         * @brief Constructor.
        */
        RemoveDirectoryCommand() = default;
        
        /**
         * @brief Destructor.
        */
        ~RemoveDirectoryCommand() = default;

        /**
         * @brief Returns the name of the command.
         * @return Name of command.
        */
        static std::string getName();

        /**
         * Executes the command.
         * @param command  [in] Name of command and arguments.
         * @return True if succeeded, false otherwise.
        */
        bool execute(const std::vector<std::string>& command);
};

} // namespace virtualfilesystem

#endif // REMOVEDIRECTORYCOMMAND_H
