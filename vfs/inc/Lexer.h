////////////////////////////////////////////////////////////////////////////////
/**
* @file Lexer.h
* @brief A class to break a string into lexemes (tokens).
*
* (C) Copyright 2018 HP Development Company, L.P.
* All rights reserved.
*/
////////////////////////////////////////////////////////////////////////////////

#ifndef LEXER_HPP
#define LEXER_HPP

#include <string>
#include <vector>

#include "Parser.h"

namespace virtualfilesystem {

/**
 * @brief A class to break a string into lexemes (tokens).
*/
class Lexer
{
    public:
        /**
         * @brief Constructor.
        */
        Lexer() = default;

        /**
         * @brief Destructor.
        */
        ~Lexer() = default;

        /**
         * @brief Reads string and store as tokens.
         * @param line [in] Text to be tokenized.
         * @return True if succeeded, false otherwise.
        */
        bool readString(const std::string& line);

        /**
         * @brief Gets the tokens from the text.
         * @return A collection of tokens.
        */
        std::vector<std::string> getTokens();
    
    private:
        /**
         * @brief Tokens from the text.
        */
        std::vector<std::string> tokens_ = std::vector<std::string>();
};

} // namespace virtualfilesystem

#endif // LEXER_HPP
