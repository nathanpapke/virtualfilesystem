////////////////////////////////////////////////////////////////////////////////
/**
* @file main.cpp
* @brief The main file.
*
* (C) Copyright 2018 HP Development Company, L.P.
* All rights reserved.
*/
////////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <memory>

#include "../inc/CommandLine.h"

//namespace virtualfilesystem {
using namespace virtualfilesystem;

int main()
{
    std::unique_ptr<CommandLine> commandLine = std::make_unique<CommandLine>();
    //std::string input;

    bool loop = true;
    while (loop)
    {
        //commandLine->prompt();
        //loop = commandLine->run(std::cin);
    }

    return 0;
}

//} // namespace virtualfilesystem
