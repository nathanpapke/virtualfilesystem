////////////////////////////////////////////////////////////////////////////////
/**
* @file ListCommand.h
* @brief A command for listing files and directories.
*
* (C) Copyright 2018 HP Development Company, L.P.
* All rights reserved.
*/
////////////////////////////////////////////////////////////////////////////////

#ifndef LISTCOMMAND_H
#define LISTCOMMAND_H

#include "ICommand.h"

namespace virtualfilesystem {

/**
 * @brief A command for listing files and directories.
*/
class ListCommand : public ICommand
{
    public:
        /**
         * @brief Constructor.
        */
        ListCommand() = default;
        
        /**
         * @brief Destructor.
        */
        ~ListCommand() = default;

        /**
         * @brief Returns the name of the command.
         * @return Name of command.
        */
        static std::string getName();

        /**
         * Executes the command.
         * @param command  [in] Name of command and arguments.
         * @return True if succeeded, false otherwise.
        */
        bool execute(const std::vector<std::string>& command);
};

} // namespace virtualfilesystem

#endif // LISTCOMMAND_H
