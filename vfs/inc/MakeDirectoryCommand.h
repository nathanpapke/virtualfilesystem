////////////////////////////////////////////////////////////////////////////////
/**
* @file MakeDirectoryCommand.h
* @brief A command for making directories.
*
* (C) Copyright 2018 HP Development Company, L.P.
* All rights reserved.
*/
////////////////////////////////////////////////////////////////////////////////

#ifndef MAKEDIRECTORYCOMMAND_H
#define MAKEDIRECTORYCOMMAND_H

#include "ICommand.h"

namespace virtualfilesystem {

/**
 * @brief A command for making directories.
*/
class MakeDirectoryCommand : public ICommand
{
    public:
        /**
         * @brief Constructor.
        */
        MakeDirectoryCommand() = default;
        
        /**
         * @brief Destructor.
        */
        ~MakeDirectoryCommand() = default;

        /**
         * @brief Returns the name of the command.
         * @return Name of command.
        */
        static std::string getName();

        /**
         * Executes the command.
         * @param command  [in] Name of command and arguments.
         * @return True if succeeded, false otherwise.
        */
        bool execute(const std::vector<std::string>& command);
};

} // namespace virtualfilesystem

#endif // MAKEDIRECTORYCOMMAND_H
