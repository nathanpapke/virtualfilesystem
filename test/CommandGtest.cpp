////////////////////////////////////////////////////////////////////////////////
/**
* @file CommandGtestMain.cpp
* @brief A test of the command program functions.
*
* (C) Copyright 2018 HP Development Company, L.P.
* All rights reserved.
*/
////////////////////////////////////////////////////////////////////////////////

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <memory>

#include "../inc/CommandLine.h"
#include "../inc/ICommand.h"
#include "../inc/Lexer.h"
#include "../inc/MockCommand.h"
#include "../inc/Parser.h"

using namespace virtualfilesystem;

class GivenCommand : public ::testing::Test
{
    protected:
        GivenCommand()
        {
            // You can do set-up work for each test here.
        }

        ~GivenCommand()
        {
            // You can do clean-up work that doesn't throw exceptions here.
        }

        void SetUp() override
        {
            // Code here will be called immediately after the constructor (right
            // before each test).
        }

        void TearDown() override
        {
            // Code here will be called immediately after each test (right
            // before the destructor).
        }

    public:
        std::shared_ptr<CommandLine> commandLine = std::make_shared<CommandLine>(); //testing public/private of objects in tests
};

/**
 * @brief Tests run method in CommandLine class.
*/
TEST_F(GivenCommand, WhenAnyInput_ShouldReturnTrue)
{
    //std::shared_ptr<CommandLine> commandLine = std::make_unique<CommandLine>();

    EXPECT_TRUE(commandLine->run(std::stringstream("any")));
}

/**
 * @brief Tests run method in CommandLine class.
*/
TEST_F(GivenCommand, WhenNoInput_ShouldReturnTrue)
{
    std::unique_ptr<CommandLine> commandLine = std::make_unique<CommandLine>();

    EXPECT_TRUE(commandLine->run(std::stringstream("")));
}

/**
 * @brief Tests run method in CommandLine class.
*/
TEST_F(GivenCommand, WhenExitInput_ShouldReturnFalse)
{
    std::unique_ptr<CommandLine> commandLine = std::make_unique<CommandLine>();

    EXPECT_FALSE(commandLine->run(std::stringstream("exit")));
}

/**
 * @brief Tests readString method in Lexor class.
*/
TEST_F(GivenCommand, WhenAnyInput_ShouldPass)
{
    std::unique_ptr<Lexer> lexer = std::make_unique<Lexer>();

    EXPECT_TRUE(lexer->readString("anything"));
}

/**
 * @brief Tests analyzeTokens method in Parser class.
*/
TEST_F(GivenCommand, WhenAnyTokens_ShouldPass)
{
    std::unique_ptr<virtualfilesystem::Parser> parser = std::make_unique<Parser>();
}

/**
 * @brief Tests getName method in ICommand interface.
*/
TEST_F(GivenCommand, WhenCommandGetName_ShouldReturnString)
{
    auto mockCommand = std::make_unique<MockCommand>(); //why auto needed?

    EXPECT_CALL(*mockCommand, getName())
        .WillOnce(::testing::Return("Learn Mock"));

    EXPECT_EQ(mockCommand->getName(), "Learn Mock");
}

/**
 * @brief Tests execute method in ICommand interface.
*/
TEST_F(GivenCommand, WhenCommandIsExecuted_ShouldReturnTrue)
{
    auto mockCommand = std::make_unique<MockCommand>();

    std::vector<std::string> args;

    EXPECT_CALL(*mockCommand, execute(args))
        .WillOnce(::testing::Return(true));

    EXPECT_TRUE(mockCommand->execute(args));
}
