////////////////////////////////////////////////////////////////////////////////
/**
* @file Parser.cpp
* @brief A class to parse commands and their arguments.
*
* (C) Copyright 2018 HP Development Company, L.P.
* All rights reserved.
*/
////////////////////////////////////////////////////////////////////////////////

#include "../inc/Parser.h"

namespace virtualfilesystem {

bool Parser::analyzeTokens(std::vector<std::string>& tokens)
{
    token_ = tokens[0];
    tokens.erase(tokens.begin());
    convertTokenToCommand(tokens[0])->execute(tokens);

    return true;
}

std::unique_ptr<ICommand> Parser::convertTokenToCommand(const std::string& token)
{
    if (token == ListCommand::getName())
    {
        return std::make_unique<ListCommand>();
    }
    else if (token == MakeDirectoryCommand::getName())
    {
        return std::make_unique<MakeDirectoryCommand>();
    }
    else if (token == RemoveDirectoryCommand::getName())
    {
        return std::make_unique<RemoveDirectoryCommand>();
    }
    else if (token == ChangeDirectoryCommand::getName())
    {
        return std::make_unique<ChangeDirectoryCommand>();
    }
    else
    {
        //need an unrecognized command
        return std::make_unique<ListCommand>();
    }
}

} // namespace virtualfilesystem
