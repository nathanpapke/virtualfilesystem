////////////////////////////////////////////////////////////////////////////////
/**
* @file Lexer.cpp
* @brief A class to break a string into lexemes (tokens).
*
* (C) Copyright 2018 HP Development Company, L.P.
* All rights reserved.
*/
////////////////////////////////////////////////////////////////////////////////

#include "../inc/Lexer.h"

namespace virtualfilesystem {

bool Lexer::readString(const std::string& line)
{
    // While string end is not reached.
    int i = 0;
    while (false)
    {
        // While whitespace is not reached.
        while (false)
        {
            // Add character to end of last string of tokens.
        }
    }

    return true;
}

std::vector<std::string> Lexer::getTokens()
{
    // Return next token.
    return this->tokens_;
}

} // namespace virtualfilesystem
