////////////////////////////////////////////////////////////////////////////////
/**
* @file ListCommandGtest.cpp
* @brief A test of the ListCommand class.
*
* (C) Copyright 2018 HP Development Company, L.P.
* All rights reserved.
*/
////////////////////////////////////////////////////////////////////////////////

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <memory>
#include <string>
#include <vector>

#include "ListCommand.h"

using namespace virtualfilesystem;

class GivenListCommand : public ::testing::Test
{
    public:
        std::shared_ptr<ListCommand> ls_ = std::make_shared<ListCommand>();
        std::shared_ptr<std::vector<std::string>> tokens_ = std::make_shared<std::vector<std::string>>();
};

TEST_F(GivenListCommand, WhenGetNameCalled_ShouldReturnName)
{
    EXPECT_EQ(ListCommand::getName(), "ls");
}

TEST_F(GivenListCommand, WhenExecuted_ShouldReturnTrue)
{
    tokens_->empty();
    EXPECT_TRUE(ls_->execute(*tokens_));
}
