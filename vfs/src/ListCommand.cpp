////////////////////////////////////////////////////////////////////////////////
/**
* @file ListCommand.cpp
* @brief A command for listing files and directories.
*
* (C) Copyright 2018 HP Development Company, L.P.
* All rights reserved.
*/
////////////////////////////////////////////////////////////////////////////////

#include "../inc/ListCommand.h"

namespace virtualfilesystem {

std::string ListCommand::getName()
{
    return "ls";
}

bool ListCommand::execute(const std::vector<std::string>& command)
{
    // Check for no remaining tokens.
        // List directories and files in current directory.

    // Check for valid directory name.
        // List directories and files in specified directory.

    // Check for invalid directory name.
        // Give error message and return false.

    // Check for null string.
        // Return false.

    return false;
}

} // namespace virtualfilesystem
