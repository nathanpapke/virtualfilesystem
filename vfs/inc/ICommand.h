////////////////////////////////////////////////////////////////////////////////
/**
* @file ICommand.h
* @brief An interface for command classes.
*
* (C) Copyright 2018 HP Development Company, L.P.
* All rights reserved.
*/
////////////////////////////////////////////////////////////////////////////////

#ifndef ICOMMAND_H
#define ICOMMAND_H

#include <string>
#include <vector>

namespace virtualfilesystem {

/**
 * @brief Interface for command classes.
*/
class ICommand
{
    public:
        /**
         * @brief Constructor.
        */
        ICommand() = default;
        
        /**
         * @brief Destructor.
        */
        ~ICommand() = default;

        /**
         * @brief Returns the name of the command.
         * @return Name of command.
        */
        static std::string getName();

        /**
         * @brief Executes the command.
         * @param command [in] Name of command and arguments.
         * @return True if succeeded, false otherwise.
        */
        virtual bool execute(const std::vector<std::string>& command) = 0;
};

} // namespace virtualfilesystem

#endif // ICOMMAND_H
