////////////////////////////////////////////////////////////////////////////////
/**
* @file ChangeDirectoryCommandGtest.cpp
* @brief A test of the MakeDirectoryCommand class.
*
* (C) Copyright 2018 HP Development Company, L.P.
* All rights reserved.
*/
////////////////////////////////////////////////////////////////////////////////

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <memory>
#include <string>
#include <vector>

#include "ChangeDirectoryCommand.h"

using namespace virtualfilesystem;

class GivenChangeDirectoryCommand : public ::testing::Test
{
    public:
        std::shared_ptr<ChangeDirectoryCommand> cd_ = std::make_shared<ChangeDirectoryCommand>();
        std::shared_ptr<std::vector<std::string>> tokens_ = std::make_shared<std::vector<std::string>>();
};

TEST_F(GivenChangeDirectoryCommand, WhenGetNameCalled_ShouldReturnName)
{
    EXPECT_EQ(ChangeDirectoryCommand::getName(), "cd");
}

TEST_F(GivenChangeDirectoryCommand, WhenExecuted_ShouldReturnTrue)
{
    tokens_->empty();
    EXPECT_TRUE(cd_->execute(*tokens_));
}
