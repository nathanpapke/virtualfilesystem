////////////////////////////////////////////////////////////////////////////////
/**
* @file MakeDirectoryCommand.cpp
* @brief A command for making directories.
*
* (C) Copyright 2018 HP Development Company, L.P.
* All rights reserved.
*/
////////////////////////////////////////////////////////////////////////////////

#include "../inc/MakeDirectoryCommand.h"

namespace virtualfilesystem {

std::string MakeDirectoryCommand::getName()
{
    return "md";
}

bool MakeDirectoryCommand::execute(const std::vector<std::string>& command)
{
    // Check for no remaining tokens.
        // Give error message and return false.

    // Check for valid directory name.
        // Iterate through token and temporarily point to specified directory.

    // Check for invalid directory name.
        // Give error message and return false.
    
    // Check for unused directory name.
        // Create new directory in current directory.

    // Check for null string.
        // Return false.

    return false;
}

} // namespace virtualfilesystem
